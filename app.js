const express = require("express");
const bodyParser = require("body-parser");
const router = require("./routes");
const app = express();

const passport = require("passport");
const passportJWT = require("passport-jwt");
const ExtractJWT = passportJWT.ExtractJwt;
const Strategy = passportJWT.Strategy;

const db = require("./models");

const PORT = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const params = {
  secretOrKey: 'asdf',
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
};

let strategy = new Strategy(params, function (payload, done) {
  db.user
    .findOne({
      where: {
        id: payload.id,
      },
    })
    .then((user) => {
      if (!user) {
        return done(new Error("User not found"), null);
      } else {
        return done(null, {
          id: user.id,
        });
      }
    })
    .catch((err) => {
      return done(err);
    });
});

passport.use(strategy);

app.use("/", router);

const server = app.listen(PORT, function () {
  console.log("port: " + server.address().port);
});
