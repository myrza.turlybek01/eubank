const express = require("express");
const router = express.Router();
const db = require("../models");
const bcrypt = require("bcryptjs");
const multer = require("multer");

const jwt = require("jwt-simple");

let passport = require("passport");

let auth = passport.authenticate("jwt", {
  session: false,
});

let salt = bcrypt.genSaltSync(10);

const isValidPassword = function (user, password) {
  return bcrypt.hashSync(password, salt) === user.password;
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads");
  },
  filename: (req, file, cb) => {
    console.log(file);
    cb(null, Date.now() + file.originalname);
  },
});
const fileFilter = (req, file, cb) => {
  if (file.mimetype == "image/jpeg" || file.mimetype == "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({ storage: storage, fileFilter: fileFilter });

router.post("/signup", (req, res, next) => {
  db.user
    .findOne({
      where: {
        name: req.body.name,
      },
    })
    .then((checkUser) => {
      if (checkUser) {
        res.json({ error: "such user already exists" });
      } else {
        db.user
          .create({
            name: req.body.name,
            password: bcrypt.hashSync(req.body.password, salt),
          })
          .then((user) => {
            res.json({ name: user.name });
          });
      }
    });
});

router.post("/login", (req, res, next) => {
  db.user
    .findOne({
      where: {
        name: req.body.name,
      },
    })
    .then((user) => {
      if (isValidPassword(user, req.body.password)) {
        let payload = {
          id: user.id,
        };
        let token = jwt.encode(payload, "asdf");
        res.json({ token: token });
      } else {
        const err = new Error("auth error");
        err.status = 400;
        next(err);
      }
    })
    .catch((err) => {
      next(err);
    });
});

router.post("/upload", auth, upload.array("images", 100), (req, res, next) => {
  let filedata = req.files;
  console.log(filedata);
  if (!filedata) res.json({ result: "Error" });
  else {
    db.user
      .findOne({
        where: {
          id: req.user.id,
        },
      })
      .then((user) => {
        filedata.forEach((file) => {
          user.createImage({
            name: file.filename,
          });
        });
      });
    res.json({ result: "success" });
  }
});

router.post("/files", auth, (req, res, next) => {
  db.user
    .findOne({
      where: {
        id: req.user.id,
      },
    })
    .then((user) => {
      user.getImages().then((images) => {
        res.json(images);
      });
    });
});

router.post("/file/:id", auth, (req, res, next) => {
  db.img
    .findOne({
      where: {
        id: req.params.id,
      },
    })
    .then((img) => {
      if (!img) {
        res.json({ error: "img not found" });
      } else {
        if (img.userId === req.user.id) {
          res.download("./uploads/" + img.name);
        } else {
          res.json({ error: "img not found" });
        }
      }
    });
});

module.exports = router;
