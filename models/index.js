const Sequelize = require("sequelize");
const sequelize = new Sequelize("eubank", "root", "Wmr68im12", {
  dialect: "mysql",
  host: "localhost",
});

const User = sequelize.define("user", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});

const Img = sequelize.define("image", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});

User.hasMany(Img, { onDelete: "cascade" });

sequelize
  .sync({ force: true })
  .then((result) => {
    // console.log(result);
  })
  .catch((err) => console.log(err));

module.exports = { user: User, img: Img };
